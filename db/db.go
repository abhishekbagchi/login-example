package db

import (
    "database/sql"
    "log"
    "os"
    "fmt"
    _ "github.com/lib/pq"
)

var db *sql.DB

func CleanupDB() {
    log.Println("CleanupDB called")
    defer db.Close()
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func CheckUser(uname string) bool {
    err := db.Ping()
    if err != nil {
        log.Printf("Fatal Error: %s", err.Error())
        os.Exit(1)
    }

    log.Printf("Checking if %s exists in the database", uname)
    rows, err := db.Query("SELECT EXISTS(SELECT * FROM userinfo WHERE username = $1) AS \"exists\";", uname)
    checkErr(err)

    defer rows.Close()
    var res bool = false
    for rows.Next() {
        err := rows.Scan(&res)
        checkErr(err)
    }

    return res
}

func CheckUserPassword(uname string, pwd string) bool {
    err := db.Ping()
    if err != nil {
        log.Printf("Fatal Error: %s", err.Error())
        os.Exit(1)
    }

    log.Printf("Checking username and password for %s", uname)
    rows, err := db.Query("SELECT EXISTS(SELECT * FROM userinfo WHERE username = $1 AND password = $2) AS \"exists\";", uname, pwd)
    checkErr(err)

    defer rows.Close()
    var res bool = false;
    for rows.Next() {
        err := rows.Scan(&res)
        checkErr(err)
    }

    return res
}

func AddUser(uname string, pwd string) {
    err := db.Ping()
    if err != nil {
        log.Printf("Fatal Error: %s", err.Error())
        os.Exit(1)
    }

    log.Printf("Adding %s to the db", uname)
    _, err = db.Exec("INSERT INTO userinfo VALUES($1, $2);", uname, pwd)
    checkErr(err)
}

func SetupDB(uname string, pwd string) {
    psqlInfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
        uname, pwd, uname)

    log.Printf("Connecting using %s", psqlInfo)

    var err error

    db, err = sql.Open("postgres", psqlInfo)
    if err != nil {
        log.Printf("Fatal Error: %s", err.Error())
        os.Exit(1)
    }
    err = db.Ping()
    if err != nil {
        log.Printf("Fatal Error: %s", err.Error())
        os.Exit(1)
    }
    log.Printf("Successfully connected to db")
}

