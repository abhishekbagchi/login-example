package main

import (
    "log"
    "flag"
    "net/http"
    "sync"
    "html/template"
    "gitlab.com/abhishekbagchi/login_example/db"
)

var templ *template.Template;

var registerError = "The username already exists"
var loginError = "The username password combination does not match"

type PageData struct {
    Logindisplay string
    Registerdisplay string
    RegisterError string
    LoginError string
    Message string
}

func register(w http.ResponseWriter, r *http.Request) {
    //Debug and informational log
    //Add data validation as and when needed
    log.Println("Register. Method: %s", r.Method)
	if (r.Method == "POST") {
        r.ParseForm()
        //for key := range r.Form {
        //    log.Println(key, r.Form[key])
        //}
        res := db.CheckUser(r.Form.Get("uname"))
        if (res == true) {
            pageData := PageData{
                Registerdisplay: "block",
                Logindisplay: "none",
                RegisterError: registerError,
            }
            templ.Execute(w, pageData)
        } else {
            db.AddUser(r.Form.Get("uname"), r.Form.Get("password"))
            pageData := PageData{
                Registerdisplay: "none",
                Logindisplay: "none",
                Message: "Successfully registered",
            }
            templ.Execute(w, pageData)
        }
	}
}

func login(w http.ResponseWriter, r *http.Request) {
    //Debug and informational log
    //Add data validation as and when needed
    log.Println("Login. Method: %s", r.Method)
	if (r.Method == "POST") {
        r.ParseForm()
        //for key := range r.Form {
        //    log.Println(key, r.Form[key])
        //}
        res := db.CheckUserPassword(r.Form.Get("uname"), r.Form.Get("password"))
        if (res == false) {
            pageData := PageData{
                Registerdisplay: "none",
                Logindisplay: "block",
                LoginError: loginError,
            }
            templ.Execute(w, pageData)
        } else {
            //db.AddUser(r.Form.Get("uname"), r.Form.Get("password"))
            pageData := PageData{
                Registerdisplay: "none",
                Logindisplay: "none",
                Message: "Successfully logged in",
            }
            templ.Execute(w, pageData)
        }

	}
}

func serveIndex(w http.ResponseWriter, r *http.Request) {
    log.Println("In serveIndex")
    pageData := PageData{
        Logindisplay: "none",
        Registerdisplay: "none",
    }
    templ.Execute(w, pageData)
}

func startServer(wg *sync.WaitGroup) {
    defer wg.Done()

    templ, _ = templ.ParseFiles("html/index.gtpl")

    http.HandleFunc("/", serveIndex)
	http.HandleFunc("/login", login)
	http.HandleFunc("/register", register)
	http.ListenAndServe(":3000", nil)
}

func main() {
    uname := flag.String("username", "", "Username")
    pwd := flag.String("password", "", "Password")
    flag.Parse()

    db.SetupDB(*uname, *pwd)

    var wg sync.WaitGroup

    go startServer(&wg)
    wg.Add(1)

    wg.Wait()
    defer db.CleanupDB()
}
